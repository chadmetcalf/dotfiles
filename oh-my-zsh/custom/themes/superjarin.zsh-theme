# Grab the current version of ruby in use: [ruby-1.8.7]
JARIN_CURRENT_RUBY_="%{$fg[white]%}[%{$fg[red]%}\$(ruby_prompt_info)%{$fg[white]%}]%{$reset_color%}"

# Grab the current filepath, use shortcuts: ~/Desktop
JARIN_CURRENT_PATH_="%{$fg[cyan]%}%~%{$reset_color%}"

# Append the current git branch, if in a git repository
JARIN_CURRENT_GIT_="\$(git_prompt_info)"
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"

if [[ -z "$SSH_CLIENT" ]]; then
  current_host=""
else
  current_host=%{$fg[yellow]%}$(hostname -s)
fi

# Do nothing if the branch is clean (no changes).
ZSH_THEME_GIT_PROMPT_CLEAN=""

# Add a red * if the branch is dirty
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[yellow]%}*"

# Put it all together!
PROMPT="$JARIN_CURRENT_RUBY_ $JARIN_CURRENT_GIT_
$current_host $JARIN_CURRENT_PATH_ $ "

